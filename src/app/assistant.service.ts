import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { assistantId, assistantUsername, assistantPassword, assistantVersion, apiBaseUrl } from '../../env'

import * as Assistant from './assistant';
@Injectable({
  providedIn: "root"
})
export class AssistantService {

  apiBaseUrl: string = apiBaseUrl
  assistantId: string = assistantId;
  assistantUsername: string = assistantUsername;
  assistantPassword: string = assistantPassword;
  assistantVersion: string = assistantVersion;

  headers: HttpHeaders;
  sessionId: string;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders()
    this.headers = this.headers.append("Authorization", "Basic " + btoa(this.assistantUsername + ':' + this.assistantPassword));
    this.headers = this.headers.append("Content-Type", "application/json");
    console.log("headers:", this.headers)
  }

  sendMessage(text: string): Observable<Assistant.Response> {
    let message = new Assistant.Request(text);
    return this.http.post<Assistant.Response>(
      `${this.apiBaseUrl}/${this.assistantId}/sessions/${this.sessionId}/message?version=${this.assistantVersion}`,
      message,
      { headers: this.headers });
  }

  createSession(): Observable<Assistant.Session> {
    return this.http.post<Assistant.Session>(
      `${this.apiBaseUrl}/${this.assistantId}/sessions?version=${this.assistantVersion}`,
      {},
      { headers: this.headers })
  }
}