export class Response {
    output: Output;
}

export class Request {
    input: Input;
    constructor(text: string){
        this.input = new Input(text)
    }
}

export class Session {
    session_id: string;
    constructor(session_id){
        this.session_id = session_id
    }
}

class Input {
    text: string;
    constructor(text){
        this.text = text;
    }
}

class Generic {
    response_type: string;
    text: string;
    options: Option[]
}

class Option {
    label: string;
    value: Value;
}

class Value {
    input: Input;
}

class Intent {
    intent: string;
    confidence: number;
}

class Entity {
    entity: string;
    location: number[];
    value: string;
    confidence: number;
}

class Output {
    generic: Generic[];
    intents: Intent[];
    entities: Entity[];
}
