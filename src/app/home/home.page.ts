import { Component, OnInit } from '@angular/core';
import { AssistantService } from '../assistant.service';
import { Message } from '../message-renderer/message';

import * as Assistant from '../assistant';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  title = "TestPWA";
  messages: Message[] = [];
  userMessage: string = "";

  constructor(private assistantService: AssistantService) { }

  ngOnInit() {
    this.initializeAssistant()
  }

  initializeAssistant() {
    this.assistantService.createSession().subscribe(result => {
      this.assistantService.sessionId = result.session_id;
      console.log(this.assistantService.sessionId)
      this.assistantService.sendMessage('').subscribe(result => {
        console.log(result)
        this.messages.push(
          new Message(
            false,
            result.output.generic[0].text,
            "Watson",
            new Date().toDateString(),
            result
          )
        );
      });
    })
  }

  sendMessage(message) {
    this.assistantService.sendMessage(message).subscribe(result => {
      console.log(result)
      this.messages.push(
        new Message(
          false,
          result.output.generic[0].text,
          "Watson",
          new Date().toDateString(),
          result
        )
      );
    });
  }

  keydownHandler(event) {
    if (event.keyCode == 13) {
      if (this.userMessage !== "") {
        this.messages.push(
          new Message(
            true,
            this.userMessage,
            "User",
            new Date().toDateString()
          )
        );
        this.sendMessage(this.userMessage);
        this.userMessage = "";
      }
    }
  }
}
