import { Component, OnInit, ViewChild, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Message } from './message';
import { from } from 'rxjs';

import * as Assistant from '../assistant';

@Component({
  selector: 'message-renderer',
  templateUrl: './message-renderer.component.html',
  styleUrls: ['./message-renderer.component.scss']
})
export class MessageRendererComponent implements OnInit {

  @Input("messages")
  messages: Message[] = [];

  @Output() responseClicked = new EventEmitter<string>();

  @ViewChild("messageContainer")
  private messageContainer: ElementRef;

  constructor() {}

  ngOnInit() {}

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  responseElementClicked(message: string){
    console.log(message)
    this.responseClicked.emit(message);
    this.messages.push(
      new Message(
        true,
        message,
        "User",
        new Date().toDateString()
      )
    )
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.messageContainer.nativeElement.scrollTop = this.messageContainer.nativeElement.scrollHeight;
    } catch (err) {
      console.log(err);
    }
  }
}
