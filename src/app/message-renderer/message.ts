import {Response} from '../assistant';

export class Message {
	isMe: boolean;
	name: string;
	message: string;
	time: string;
	response: Response;

	constructor(isMe: boolean, message: string, name?: string, time?: string, response?:Response) {
		this.isMe = isMe;
		this.message = message;
		this.name = name;
		this.time = time;
		this.response = response;
	}
}