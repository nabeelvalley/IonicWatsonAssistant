## API Config

Endpoint configuration and API credentials can be placed in the `env.ts` file, to create this simply copy the `env.default.ts` file and edit the necessary fields. When running from the browser use the following value for the `apiBaseUrl`

```ts
export const apiBaseUrl = "/assistant/api/v2/assistants" // for local
```

When building for production be sure to change this to use the correct endpoint of

```ts
export const apiBaseUrl = "https://gateway.watsonplatform.net/assistant/api/v2/assistants"; // for prod
```

The rest of your environment variables also need to be defined in th e`env.ts` file as below

```ts
export const apiBaseUrl = "/assistant/api/v2/assistants" // for local
// export const apiBaseUrl = "https://gateway.watsonplatform.net/assistant/api/v2/assistants"; // for prod
export const assistantId = "xxx-xxxx-xxx"; 
export const assistantUsername = "xxx-xxx-xxx";
export const assistantPassword = "xxx-xxx-xxx";
export const assistantVersion = "2018-11-08";
```

# Install Dependencies

In order to run the application you need to do the following

```
npm i -g ionic
npm install
```

# Run the Application

In order to run the app locally via the browser with the Angular Proxy run the following command

```bash
npm run start-proxy
```

# Building the Application

## Install Dependencies

```
npm i -g cordova
ionic cordova platform update android ios
ionic cordova plugin add cordova-plugin-mfp
```

## Building the Application for Production

### Android

```
ionic cordova build android --prod
```

### IOS

If you are running ths on an ios device you will need to run your commands prefixed with `sudo`

```
ionic cordova build ios --prod
```

# Deploy the Application

## Android Application

When deploying for Android you need to open the Android project in the `platforms/android` folder with Android Studio and build the APK

## Deploying IOS

To Deploy an iOS Application open the `.xcodeproj` file in the `platforms/ios` with XCode and Run on application or do a formal deployment (which will require an Apple Developer Subscription or an Apple Enteprise Developer Subscription)

# Troubleshooting

If during the run build process you get the following error:

```
cordova could not reserve enough space for 2097152KB object heap
```

You need to change the JVM memory allocation by following this [Stackoverflow Answer](https://stackoverflow.com/questions/41216921/cordova-could-not-reserve-enough-space-for-2097152kb-object-heap) or [here](https://stackoverflow.com/questions/4401396/could-not-reserve-enough-space-for-object-heap/30090271#30090271), as follows:

Try again after increasing your Java VM(?) memory size.

Here is how to fix it on a Windows platform:

1. Go to Start -> Control Panel -> System -> Advanced(tab) -> 2. Environment Variables -> System Variables -> New:
2. Variable name: `_JAVA_OPTIONS`
3. Variable value: `-Xmx512M`

And ensure that your Java `PATH` variable is also set

Then restart your terminal and try to do the build again

# Application Structure

## Component Outline

The functional code for the application resides in a few simple components

- The *Home Component* manages the instantiation of the assistant session and the sending and receiving of messages by way of the Message Service and handles option-selection input events from the Message Renderer Component
- The *Message Renderer Component* renders the messages based on type and user as well as handles option inputs and emits them up to the Home Component to manage (mainly because I didn't want multiple components modifying message state)
- The *Assistant Service* makes the API Calls to the Assistant as well as reads the `env.ts` file for configuration info. This component also persists the Session ID for the conversation
- The `assistant.ts` file contains the necessary type definitions for objects being handled by the API (mainly to keep track of the structure of the requests)
- The`message.ts` file contains the definition for the structure of messages
