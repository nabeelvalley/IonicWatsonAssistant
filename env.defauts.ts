export const apiBaseUrl = "/assistant/api/v2/assistants" // for local
// export const apiBaseUrl = "https://gateway.watsonplatform.net/assistant/api/v2/assistants"; // for prod
export const assistantId = "xxx-xxxx-xxx"; 
export const assistantUsername = "xxx-xxx-xxx";
export const assistantPassword = "xxx-xxx-xxx";
export const assistantVersion = "2018-11-08";